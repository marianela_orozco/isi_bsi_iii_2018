jQuery(document).ready(function($){

            $("#insertar").click(function(){

                var v_nombre = $("#nombre").val();
                var v_identificacion = $("#identificacion").val();
                var v_direccion = $("#direccion").val();
                var v_telefono = $("#telefono").val();
               

                //Expresión para validar numeros
                var expr = /^([0-9])*$/;

                if(v_nombre == "" || v_identificacion == "" || v_direccion == "" || v_telefono == ""){
                    alert("Debe completar todos los datos para insertar persona.");
                    return false;
                } else {
                    if (!expr.test(v_identificacion) || !expr.test(v_telefono)) {
                        alert("La identificacion y el numero de telefono deben ser numericos.");
                        return false;
                    } else {
                       
                        var params = {
                        "nombre" : v_nombre,
                        "identificacion" : v_identificacion,
                        "direccion" : v_direccion,
                        "telefono" : v_telefono,
                      
                        }   
                        
                        $.ajax({
                            type: "POST",
                            url: "insertar.php",
                            data: params
                        }).done(function(msg){
                            $("#nombre").val("");
                            $("#identificacion").val("");
                            $("#direccion").val("");
                            $("#telefono").val("");
                            

                             $("#resultado").html(msg);
                        });
                    }
                }

                
            
        }); 

        //mantenimiento de buscar personas
            $("#buscar").click(function(){

                var v_nombre = $("#nombre").val();
                var v_identificacion = $("#identificacion").val();
                var v_Query = "";

                //Expresión para validar numeros
                var expr = /^([0-9])*$/;

                if(v_nombre == "" && v_identificacion == ""){
                    alert("Debe completar el nombre o identificacion de la persona para proceder con la busqueda.");
                    return false;
                } else {

                    var entra = 0;
                    v_Query = "Select * from clientes where ";

                    if (v_identificacion != "" && !expr.test(v_identificacion)) {
                        alert("La cedula debe ser numerica.");
                        return false;
                    } else {

                        if (v_identificacion != "") {
                            entra = 1;
                            v_Query = v_Query + "identificacion=" + v_identificacion;
                        }
                        
                    }

                    if (v_nombre != ""){
                        if (entra == 1) {
                           v_Query = v_Query + " and "; 
                        } 
                        v_Query = v_Query + "nombre = '" + v_nombre + "'";
                    }

                    var params = {
                        "Query" : v_Query
                        }   
                        
                        $.ajax({
                            type: "POST",
                            url: "buscar.php",
                            data: params
                        }).done(function(msg){
                            
                            $("#resultado").html("Busqueda completada.");
                            var res = msg.split("*");
                                                
                            $("#nombre").val(res[1]);
                            $("#identificacion").val(res[2]);
                            $("#direccion").val(res[3]); 
                            $("#telefono").val(res[4]);      
                            
                        });
                }

                
            
        }); 

         //mantenimiento de actualizar  persona
            $("#actualizar").click(function(){

                
                var v_nombre = $("#nombre").val();
                var v_identificacion = $("#identificacion").val();
                var v_direccion  = $("#direccion").val();
                var v_telefono = $("#telefono").val();
                
                //Expresión para validar numeros
                var expr = /^([0-9])*$/;

                if(v_nombre == "" ||  v_tipo == "" || v_identificacion  == "" ||  v_direccion  == "" || v_contacto == "" ||v_telefono == ""||v_email == ""){
                    alert("Debe completar todos los datos para actualizar persona.");
                    return false;
                } else {
                    if (!expr.test( v_identificacion) || !expr.test( v_telefono)) {
                        alert("La identificacion y el numero de telefono deben ser numericos.");
                        return false;
                    } else {
                       
                        var params = {
                        "nombre" : v_nombre,
                        "identificacion" : v_identificacion,
                        "direccion" : v_direccion,
                        "telefono" : v_telefono,
                        
                        }   
                        
                        $.ajax({
                            type: "POST",
                            url: "actualizar.php",
                            data: params
                     }).done(function(msg){
                            
                            $("#nombre").val("");
                            $("#identificacion").val("");
                            $("#direccion").val("");
                            $("#telefono").val("");
                            
                             $("#resultado").html(msg);
                        });
                    }
                }

                
            
        }); 

         //mantenimiento de eliminar  persona
            $("#eliminar").click(function(){

                var v_identificacion= $("#identificacion").val();
               
                if(v_identificacion == ""){
                    alert("Debe indicar el numero de identificacion.");
                    return false;
                } else {
                    
                        var params = {
                        "identificacion" : v_identificacion
                        }   
                        
                        $.ajax({
                            type: "POST",
                            url: "eliminar.php",
                            data: params
                        }).done(function(msg){
                         $("#nombre").val("");
                         $("#identificacion").val("");
                         $("#direccion").val("");
                         $("#telefono").val("");
                            
                             $("#resultado").html(msg);
                        });
                    }

        });

});