-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-12-2018 a las 07:40:06
-- Versión del servidor: 10.1.29-MariaDB
-- Versión de PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tarea`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `BuscarPersona` (IN `v_identificacion` INTEGER)  BEGIN 

Select * FROM persona where identificacion = v_identificacion;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `EliminaPersona` (IN `v_identificacion` INTEGER)  BEGIN 

DELETE FROM persona WHERE identificacion = v_identificacion;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `InsertarPersona` (IN `v_nombre` VARCHAR(50), IN `v_identificacion` INT(10), IN `v_direccion` VARCHAR(300), IN `v_telefono` INT(10))  BEGIN 

INSERT INTO persona (nombre,  identificacion, direccion, telefono) 
VALUES (v_nombre,v_identificacion, v_direccion, v_telefono); 

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `UpdatePersona` (IN `v_nombre` VARCHAR(200), IN `v_identificacion` VARCHAR(10), IN `v_direccion` VARCHAR(300), IN `v_telefono` INT(10))  BEGIN 

IF(SELECT Count(*) FROM persona WHERE identificacion = v_identificacion >= 1) THEN
	UPDATE persona SET nombre=v_nombre, identificacion=v_identificacion, direccion=v_direccion,  telefono=v_telefono WHERE identificacion = v_identificacion;
END IF;

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `identificacion` int(10) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `direccion` varchar(300) NOT NULL,
  `telefono` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`identificacion`, `nombre`, `direccion`, `telefono`) VALUES
(303540858, 'Marianela Orozco', 'Cartago', 12345678);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usarios`
--

CREATE TABLE `usarios` (
  `nombre` varchar(1000) NOT NULL,
  `usuario` varchar(50) NOT NULL,
  `contrasenia` varchar(20) NOT NULL,
  `rol` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usarios`
--

INSERT INTO `usarios` (`nombre`, `usuario`, `contrasenia`, `rol`) VALUES
('Marianela Orozco', 'nela1', '123', 'administrador');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
