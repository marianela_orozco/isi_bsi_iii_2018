﻿
<html>
<head>
<title>Calculadora</title>
<script src="https://ajax.googleapis.com/ajax/libs/prototype/1.7.3.0/prototype.js"></script>
<script type="text/javascript">
	
	
	function calcularJquery(){
		//recuperar datos
		var val1 = document.getElementById("valor1").value;
		var val2 = document.getElementById("valor2").value;
		var op = document.getElementById("operacion").value;
		
		
		new Ajax.Request("calculadora.php?valor1=" + val1 + "&valor2=" + val2 + "&operacion=" + op, {
			method:'get',
			onSuccess: function(transport) {
				var response = transport.responseText || "no response text";
				document.getElementById("contenedor_resultados").innerHTML = response;
			},
			onFailure: function() { 
				document.getElementById("contenedor_resultados").innerHTML = "Ha ocurrido un error!";
			}
		});
		
	}
	

	
</script>
</head>
<style type="text/css">
.texto {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-style: normal;
	font-weight: bold;
	color: #003;
}
</style>
<body>
 <h3 align="center" class="texto">CALCULADORA</h3>
  <form action="Calculadora_JQuery.php" method="POST">
 <table border="1" align="center" width="400px" cellpadding="4" class="texto">	
 <tr>	
 <td><b>Valor 1:</b></td>
		<td>
			<input type="number" name="valor1" id="valor1" size="10">
		</td>
    </tr>
	<tr>	
 <td><b>Valor 2:</b></td>
		<td>
			<input type="number" name="valor2" id="valor2" size="10">
		</td>
    </tr>
	 <tr>		
		<td><b>Operacion:</b></td>
		<td>
            <select name="operacion" id="operacion">
                <option>sumar</option>
                <option>restar</option>
                <option>multiplicar</option>
                <option>dividir</option>
            </select>
		</td>
    </tr>
	 <tr>		
		<td colspan="2" align="center">
		  <input type="button" name="enviar" value="Resultado" onClick="calcularJquery();"></input>
		</td>
    </tr>
	
	<tr>
		<td colspan="2" align="center">
		 <div id="contenedor_resultados"></div>
		</td>
    </tr>

 </table>
</form>


</body>
</html>